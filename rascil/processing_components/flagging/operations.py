"""
Functions that Flagging Visibility and Visibility.

The flags of Visibility has axes [chan, pol, z, y, x] where z, y, x are spatial axes in either sky or Fourier plane. The
order in the WCS is reversed so the grid_WCS describes UU, VV, WW, STOKES, FREQ axes.


"""

__all__ = ["flagging_visibility", "flagging_aoflagger"]

import logging
import os
import pkgutil

import aoflagger
from astropy.wcs import WCS
import numpy

from rascil.data_models.memory_data_models import Visibility, FlagTable

log = logging.getLogger("rascil-logger")


def flagging_visibility(
    bvis, baselines=None, antennas=None, channels=None, polarisations=None
):
    """Flagging Visibility

    :param bvis: Visibility
    :param baselines: The list of baseline numbers to flag
    :param antennas: The list of antenna number to flag
    :param channels: The list of Channel number to flag
    :param polarisations: The list of polarisations to flag
    :return: Visibility
    """
    if polarisations is None:
        polarisations = []
    if antennas is None:
        antennas = []
    if channels is None:
        channels = []
    if baselines is None:
        baselines = []

    for baseline in baselines:
        bvis["flags"].data[:, baseline, ...] = 1
    for channel in channels:
        bvis["flags"].data[..., channel, :] = 1
    for pol in polarisations:
        bvis["flags"].data[..., pol] = 1
    for ibaseline, (a1, a2) in enumerate(bvis.baselines.data):
        if a1 in antennas or a2 in antennas:
            bvis["flags"].data[:, ibaseline, ...] = 1

    return bvis


def flagging_aoflagger(vis, strategy_filename):
    """Flagging Visibility using the AOFlagger package
    This uses a generic flagging strategy (from https://gitlab.com/aroffringa/aoflagger/-/tree/master/data/strategies).
    You can define a new strategy interactively using the AOFlagger rfigui (https://aoflagger.readthedocs.io/en/latest/using_rfigui.html)

    :param vis: Visibility object
    :return: Visibility where the flags field has been updated
    """
    try:
        strategy_bytes = pkgutil.get_data(
            "aoflagger", f"strategies/{strategy_filename}"
        )
    except FileNotFoundError:
        log.warning(
            f"flagging_aoflagger: {strategy_filename} not found in aoflagger package. Flagging operation is skipped."
        )
        return vis
    with open("tmp_strategy.lua", "w") as file:
        file.write(strategy_bytes.decode("utf-8"))

    flagger = aoflagger.AOFlagger()
    strategy = flagger.load_strategy_file("tmp_strategy.lua")

    ntimes = vis.vis.data.shape[0]
    nbaselines = vis.vis.data.shape[1]
    nch = vis.vis.data.shape[2]
    npol = vis.vis.data.shape[3]

    for bl in range(nbaselines):
        data = flagger.make_image_set(ntimes, nch, npol * 2)
        for imgindex in range(npol):
            # AOFlagger uses the axis in a different order than Rascil
            values = numpy.swapaxes(vis.vis.data[:, bl, :, imgindex], 0, 1)
            data.set_image_buffer(2 * imgindex, numpy.real(values))
            data.set_image_buffer(2 * imgindex + 1, numpy.imag(values))

        flags = strategy.run(data)

        # A flag denotes that the values at that time-frequency position should be ignored for all polarizations
        for pol in range(npol):
            vis.flags.data[:, bl, :, pol] = numpy.swapaxes(
                flags.get_buffer().astype(numpy.int32), 0, 1
            )

    os.remove("tmp_strategy.lua")
    return vis
